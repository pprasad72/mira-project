import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import distance

# ----------------------------------------------------------------------------#

# Extract coordinates for Inhale
    # input: path of txt file of coordinates
    # output: array of arrays of the coordinates
def get_in_coord(inhale_txt_path):
    # reading Inhale coord
    with open(inhale_txt_path, 'r') as f:
        inhale_coord = f.read().splitlines()
    f.close()
    
    for i in np.arange(0,len(inhale_coord)):
        inhale_coord[i] = inhale_coord[i].replace('\t',' ')
    
    inhale_list = []
    
    for item in  inhale_coord:
        #    print(item.split(' '))
        x = int(item.split(' ')[0])
        y = int(item.split(' ')[1])
        z = int(item.split(' ')[2])
        
        inhale_list.append([x,y,z])
    
    return inhale_list   

# ----------------------------------------------------------------------------#
    
# Extract coordinates for Exhale
    # input: path of txt file of coordinates
    # output: array of arrays of the coordinates
def get_ex_coord(exhale_txt_path):
    # reading Exhale coord
    with open(exhale_txt_path, 'r') as f:
        exhale_coord = f.read().splitlines()
    f.close()
    
    for i in np.arange(0,len(exhale_coord)):
        exhale_coord[i] = exhale_coord[i].replace('\t','').strip()
    
    exhale_list = []
    
    for item in  exhale_coord:
        #    print(item.split(' '))
        x = int(item.split(' ')[0].split('.')[0])
        y = int(item.split(' ')[1].split('.')[0])
        z = int(item.split(' ')[2].split('.')[0])
        
        exhale_list.append([x,y,z])
        
    return exhale_list

# ----------------------------------------------------------------------------#

# Find the errors between the coordinates
    # input: coordinates list of inhale and exhale
    # output: list of distances between each point, and average error
def find_error(inhale_list, exhale_list):
    all_errors  = []
    
    for i in np.arange(0, len(inhale_list)):
        i_coord = inhale_list[i]
        e_coord = exhale_list[i]
        
        dst = distance.euclidean(i_coord, e_coord)
        
        all_errors.append(dst)
        
    return all_errors, np.mean(all_errors)

# ----------------------------------------------------------------------------#

# generate dictionary of landmarks for every slice
    # input: list of coordinates
    # output: dictionary 
        # key: slice containing landmark
        # value: list of landmark coordinates 
def get_dict(list_):
    
    dict_ = {}
    
    for i in np.arange(len(list_)):
        temp = list_[i]
            
        if str(temp[2]) not in dict_.keys():
            dict_[str(temp[2])] = []
            dict_[str(temp[2])].append((temp[0], temp[1])) 
    
        else:
            dict_[str(temp[2])].append((temp[0], temp[1])) 
            
    return dict_

# ----------------------------------------------------------------------------#

# displaying a given slice along with landmarks if available
    # input: 3d image volume, slice number, and dictionary of landmark points
    # output: displayed image slice with landmarks if available 
def display_with_landmark(img_volume, slice_num, points_dict):
    
    landmark_flag = False  # to see if points exist in asked slice
    
    if str(slice_num + 1) in points_dict.keys():
        
        landmark_flag = True
        
        coords_in_slice = points_dict[str(slice_num + 1)]
        img_slice = img_volume[:,:,slice_num]
        
        for pair in coords_in_slice:
            x = pair[0]
            y = pair[1]
               
            plt.scatter(x, y, s=20, c = 'green')
            
        plt.imshow(img_slice, 'gray')

        return landmark_flag
    
    else:
        
        img_slice = img_volume[:,:,slice_num]
        plt.imshow(img_slice, 'gray')
        
        return landmark_flag

# ----------------------------------------------------------------------------#

# displaying both slices of inhale and exhale along with landmarks if available
    # input: 3d image volumes (both), slice number, and dictionary of landmark points (both)
    # output: 1 x 2 image displaying both slices with landmarks if available 
def display_with_landmark_both_volumes(in_img_volume, ex_img_volume, slice_num, in_points_dict, ex_points_dict):
    
    in_landmark_flag = False
    ex_landmark_flag = False
    
    
    f = plt.figure()
    f.add_subplot(1,2, 1)
    
    if str(slice_num + 1) in in_points_dict.keys():
            
            in_landmark_flag = True
            
            coords_in_slice = in_points_dict[str(slice_num + 1)]
            in_img_slice = in_img_volume[:,:,slice_num]
            
            for pair in coords_in_slice:
                x_i = pair[0]
                y_i = pair[1]
                   
                plt.scatter(x_i, y_i, s=20, c = 'green')
    plt.title('Inhale')            
    plt.imshow(in_img_slice,'gray')
    
    
    f.add_subplot(1,2, 2)
    
    if str(slice_num + 1) in ex_points_dict.keys():
            
            ex_landmark_flag = True
            
            coords_ex_slice = ex_points_dict[str(slice_num + 1)]
            ex_img_slice = ex_img_volume[:,:,slice_num]
            
            for pair in coords_ex_slice:
                x_e = pair[0]
                y_e = pair[1]
                   
                plt.scatter(x_e, y_e, s=20, c = 'green')
    plt.title('Exhale')
    plt.imshow(ex_img_slice,'gray')
    
    plt.show()
    
    return in_landmark_flag, ex_landmark_flag
        
# ----------------------------------------------------------------------------#        
        

    
    
    
    
    
    
    

