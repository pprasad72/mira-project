#import matplotlib.pyplot as plt
import numpy as np
import nibabel as nib
import SimpleITK as sitk
import os
import sys
from my_functions import get_in_coord, get_ex_coord, find_error, get_dict, display_with_landmark, display_with_landmark_both_volumes
                         
from utility_functions import display_images, display_images_with_alpha, start_plot, end_plot, plot_values, update_multires_iterations

#----------------------Set Path, Get Coords, Find Errors ------------------------------------------------------#
# set paths of the coordinate txt files
inhale_txt_path = "train12\copd1\copd1_300_iBH_xyz_r1.txt"
exhale_txt_path = "train12\copd1\copd1_300_eBH_xyz_r1.txt"    
 
# call function to get coordinates from the txt files
inhale_coord = get_in_coord(inhale_txt_path = inhale_txt_path)
exhale_coord = get_ex_coord(exhale_txt_path = exhale_txt_path)
 
# function call to get the errors and the average error
errors_list, avg_error  = find_error(inhale_list = inhale_coord, exhale_list = exhale_coord)



#---------------- Read 3D volume and and Display with Landmarks ------------#    
# set data path
in_data_path = "train12\copd1\copd1_iBHCT.nii.gz"
ex_data_path = "train12\copd1\copd1_eBHCT.nii.gz"

# Load NIFT image: https://nipy.org/nibabel/gettingstarted.html
img_in = nib.load(in_data_path)
img_ex = nib.load(ex_data_path)
# img_in.shape
# img_ex.shape


# Get the header information of both 3D volumes:
header_in = img_in.header
header_ex = img_ex.header

img_in_dtype = img_in.get_data_dtype()
img_ex_dtype = img_ex.get_data_dtype()
# img_in_dtype
# img_ex_dtype

# get image data as numpy array 
img_in_data = img_in.get_fdata()
img_ex_data = img_ex.get_fdata()
# plt.imshow(img_in_data[:,:,100], 'gray')
# plt.imshow(img_ex_data[:,:,100], 'gray')


# rotate image volume anti-clockwise 90 degrees, 'k' number of times
img_in_data_rot = np.rot90(img_in_data, k = 3)
img_ex_data_rot = np.rot90(img_ex_data, k = 3)
# plt.imshow(img_in_data_rot[:,:,100], 'gray')
# plt.imshow(img_ex_data_rot[:,:,100], 'gray')




#-------------------------- (Single) Displaying Points on Slice -------------------------------#

# Step 1: Get the coordinates of points for every slice
inhale_points_dict = get_dict(inhale_coord)
exhale_points_dict = get_dict(exhale_coord)

# Step 2: Given a slice number, find if landmarks are available and display them     
slice_num = 74   # from 1 - 121


# For Inhale:
flag_in = display_with_landmark(img_volume = img_in_data_rot, 
                      slice_num = slice_num - 1, 
                      points_dict = inhale_points_dict)


# For Exhale:
flag_ex = display_with_landmark(img_volume = img_ex_data_rot, 
                      slice_num = slice_num - 1, 
                      points_dict = exhale_points_dict)




#-------------------------- (Double) Displaying Points on Slice -------------------------------#

in_flag, ex_flag = display_with_landmark_both_volumes(in_img_volume = img_in_data_rot, 
                                                      ex_img_volume = img_ex_data_rot, 
                                                      slice_num = slice_num - 1 , 
                                                      in_points_dict = inhale_points_dict, 
                                                      ex_points_dict = exhale_points_dict)



#-------------------------- Impose Landmarks on 3D volume -------------------------------#
#landmarked_3d_volume = impose_landmarks_on_3D(img_volume = img_in_data_rot, points_dict = inhale_points_dict)




#-------------------------- Blank volume with landmarks -------------------------------#

# getting the moving image dimensions (this  case the exhale image volume)
x_axis, y_axis, z_axis = img_ex_data.shape 

# create empty volume of the same shape of the moving image volume:
landmark_volume = np.zeros((x_axis, y_axis, z_axis), dtype = 'int').astype('int16')

# use variable that stores the coordinates:  exhale_points_dict
all_keys = list(exhale_points_dict.keys())

# count number of landmarks added
count_landmarks = 0

# mark 1's in all landmark locations in empty volume
for key in all_keys:
    all_marks = exhale_points_dict[key] 

    for mark in all_marks:
        x_blank = mark[0]
        y_blank = mark[1]
        z_blank = int(key)
        
        landmark_volume[x_blank, y_blank, z_blank] = 1
        
        count_landmarks = count_landmarks + 1

# convert numpy array to nifti image            
nifti_landmark_volume = nib.Nifti1Image(landmark_volume, affine=np.eye(4))

# update header information to the original image
for header in list(header_ex.keys()):
    nifti_landmark_volume.header[header] = header_ex[header]

# save to file:
nib.save(nifti_landmark_volume, os.path.join('train12','copd1', '3D_landmark_before_reg', '3D_landmark.nii.gz'))  

# Next:  Use Transformix to transform the above saved image
        


#-------------------------- Extract Landmarks from Transformed Image -------------------------------#
# Note: using the same blank volume to extract landmarks to see if the same coordinates are extracted

# read the blank volume with landmarks:
landmark_vol = nib.load(os.path.join('train12','copd1', '3D_landmark_before_reg', '3D_landmark.nii.gz'))

landmark_vol_data = landmark_vol.get_fdata()

# find the locations of the landmarks (result in array of arrays of x, y, z coordinates)
all_landmarks = np.where(landmark_vol_data == 1)

# covert above to single list of coordinates
all_landmark_list = []

for i in np.arange(len(all_landmarks[0])):
    temp_x = all_landmarks[0][i]
    temp_y = all_landmarks[1][i]
    temp_z = all_landmarks[2][i]
    
    all_landmark_list.append([temp_x, temp_y, temp_z])

dict_retrieved_landmarks = get_dict(all_landmark_list)

# compute the error:
e, avg = find_error(all_landmark_list, exhale_coord)

# check if the retrieved dictionary is the same as the original one
# original: exhale_points_dict['86']
# retrieved: dict_retrieved_landmarks['86']








#==============================================================================
#==============================================================================
#==============================================================================
#===============================================================================

#-------------------------- ANTS Registration -------------------------------#
# https://antspy.readthedocs.io/en/latest/registration.html

import ants

fi = ants.image_read('train12\copd1\copd1_iBHCT.nii.gz')









#-------------------------- Free Form Deformation (FFD) -------------------------------#
# http://insightsoftwareconsortium.github.io/SimpleITK-Notebooks/Python_html/65_Registration_FFD.html

import registration_utilities as ru
import registration_callbacks as rc



def bspline_intra_modal_registration(fixed_image, moving_image, fixed_image_mask=None, fixed_points=None, moving_points=None):

    registration_method = sitk.ImageRegistrationMethod()
    
    # Determine the number of BSpline control points using the physical spacing we want for the control grid. 
    grid_physical_spacing = [50.0, 50.0, 50.0] # A control point every 50mm
    image_physical_size = [size*spacing for size,spacing in zip(fixed_image.GetSize(), fixed_image.GetSpacing())]
    mesh_size = [int(image_size/grid_spacing + 0.5) \
                 for image_size,grid_spacing in zip(image_physical_size,grid_physical_spacing)]

    initial_transform = sitk.BSplineTransformInitializer(image1 = fixed_image, 
                                                         transformDomainMeshSize = mesh_size, order=3)    
    registration_method.SetInitialTransform(initial_transform)
        
    registration_method.SetMetricAsMeanSquares()
    # Settings for metric sampling, usage of a mask is optional. When given a mask the sample points will be 
    # generated inside that region. Also, this implicitly speeds things up as the mask is smaller than the
    # whole image.
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    if fixed_image_mask:
        registration_method.SetMetricFixedMask(fixed_image_mask)
    
    # Multi-resolution framework.            
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors = [4,2,1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2,1,0])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    registration_method.SetInterpolator(sitk.sitkLinear)
    registration_method.SetOptimizerAsLBFGSB(gradientConvergenceTolerance=1e-5, numberOfIterations=100)
    

    # If corresponding points in the fixed and moving image are given then we display the similarity metric
    # and the TRE during the registration.
    if fixed_points and moving_points:
        registration_method.AddCommand(sitk.sitkStartEvent, rc.metric_and_reference_start_plot)
        registration_method.AddCommand(sitk.sitkEndEvent, rc.metric_and_reference_end_plot)
        registration_method.AddCommand(sitk.sitkIterationEvent, lambda: rc.metric_and_reference_plot_values(registration_method, fixed_points, moving_points))
    
    return registration_method.Execute(fixed_image, moving_image)

fixed_image = sitk.ReadImage('train12\copd1\copd1_iBHCT.nii.gz', sitk.sitkFloat32)
moving_image = sitk.ReadImage('train12\copd1\copd1_eBHCT.nii.gz', sitk.sitkFloat32)

tx = bspline_intra_modal_registration(fixed_image = fixed_image, 
                                      moving_image = moving_image)
                                      





#-------------------------- Registration with SITK 1 ----------------------------------------------------#
# http://insightsoftwareconsortium.github.io/SimpleITK-Notebooks/Python_html/60_Registration_Introduction.html

fixed_image =  sitk.ReadImage('train12\copd1\copd1_iBHCT.nii.gz', sitk.sitkFloat32)
moving_image = sitk.ReadImage('train12\copd1\copd1_eBHCT.nii.gz', sitk.sitkFloat32) 

# Use the CenteredTransformInitializer to align the centers of the two volumes 
#     and set the center of rotation to the center of the fixed image.
initial_transform = sitk.CenteredTransformInitializer(fixed_image, 
                                                      moving_image, 
                                                      sitk.Euler3DTransform(), 
                                                      sitk.CenteredTransformInitializerFilter.GEOMETRY)


moving_resampled = sitk.Resample(moving_image, fixed_image, initial_transform, sitk.sitkLinear, 0.0, moving_image.GetPixelID())


registration_method = sitk.ImageRegistrationMethod()

# Similarity metric settings.
registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=50)
registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
registration_method.SetMetricSamplingPercentage(0.01)

registration_method.SetInterpolator(sitk.sitkLinear)

# Optimizer settings.
registration_method.SetOptimizerAsGradientDescent(learningRate=1.0, numberOfIterations=100, convergenceMinimumValue=1e-6, convergenceWindowSize=10)
registration_method.SetOptimizerScalesFromPhysicalShift()

# Setup for the multi-resolution framework.            
registration_method.SetShrinkFactorsPerLevel(shrinkFactors = [4,2,1])
registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2,1,0])
registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

# Don't optimize in-place, we would possibly like to run this cell multiple times.
registration_method.SetInitialTransform(initial_transform, inPlace=False)

registration_method.AddCommand(sitk.sitkStartEvent, start_plot)
registration_method.AddCommand(sitk.sitkEndEvent, end_plot)
registration_method.AddCommand(sitk.sitkMultiResolutionIterationEvent, update_multires_iterations) 
registration_method.AddCommand(sitk.sitkIterationEvent, lambda: plot_values(registration_method))

final_transform = registration_method.Execute(sitk.Cast(fixed_image, sitk.sitkFloat32), 
                                              sitk.Cast(moving_image, sitk.sitkFloat32))


print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
print('Optimizer\'s stopping condition, {0}'.format(registration_method.GetOptimizerStopConditionDescription()))


moving_resampled = sitk.Resample(moving_image, fixed_image, final_transform, sitk.sitkLinear, 0.0, moving_image.GetPixelID())

sitk.WriteImage(moving_resampled, os.path.join('train12\copd1\sitk_output', 'RIRE_training_001_mr_T1_resampled.mha'))
sitk.WriteTransform(final_transform, os.path.join('train12\copd1\sitk_output', 'RIRE_training_001_CT_2_mr_T1.tfm'))


#-------------------------- Registration with SITK 2 ----------------------------------------------------#
#https://simpleitk.readthedocs.io/en/master/Examples/ImageRegistrationMethodBSpline1/Documentation.html

#def command_iteration(method) :
#    print("{0:3} = {1:10.5f}".format(method.GetOptimizerIteration(),
#                                     method.GetMetricValue()))
#
#
#fixed =  sitk.ReadImage('train12\copd1\copd1_iBHCT.nii.gz', sitk.sitkFloat32)
#moving = sitk.ReadImage('train12\copd1\copd1_eBHCT.nii.gz', sitk.sitkFloat32) 
##ct_scan = sitk.GetArrayFromImage(fixed_image)
#
#transformDomainMeshSize=[8]*moving.GetDimension()
#tx = sitk.BSplineTransformInitializer(fixed,
#                                      transformDomainMeshSize )
#
#print("Initial Parameters:");
#print(tx.GetParameters())
#
#R = sitk.ImageRegistrationMethod()
#R.SetMetricAsCorrelation()
#
#R.SetOptimizerAsLBFGSB(gradientConvergenceTolerance=1e-3,
#                       numberOfIterations=100,
#                       maximumNumberOfCorrections=5,
#                       maximumNumberOfFunctionEvaluations=100,
#                       costFunctionConvergenceFactor=1e+7)
#R.SetInitialTransform(tx, True)
#R.SetInterpolator(sitk.sitkLinear)
#
#R.AddCommand( sitk.sitkIterationEvent, lambda: command_iteration(R) )
#
#outTx = R.Execute(fixed, moving)
#
#print("-------")
#print(outTx)
#print("Optimizer stop condition: {0}".format(R.GetOptimizerStopConditionDescription()))
#print(" Iteration: {0}".format(R.GetOptimizerIteration()))
#print(" Metric value: {0}".format(R.GetMetricValue()))
#
#sitk.WriteTransform(outTx,  sys.argv[3])
#
#if ( not "SITK_NOSHOW" in os.environ ):
#
#    resampler = sitk.ResampleImageFilter()
#    resampler.SetReferenceImage(fixed);
#    resampler.SetInterpolator(sitk.sitkLinear)
#    resampler.SetDefaultPixelValue(100)
#    resampler.SetTransform(outTx)
#
#    out = resampler.Execute(moving)
#    simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
#    simg2 = sitk.Cast(sitk.RescaleIntensity(out), sitk.sitkUInt8)
#    cimg = sitk.Compose(simg1, simg2, simg1//2.+simg2//2.)
#    sitk.Show( cimg, "ImageRegistration1 Composition" )
#









#--------------------------------  Image similarity--------------------------------------#
#  https://nipype.readthedocs.io/en/latest/interfaces/generated/interfaces.ants/registration.html

from nipype.interfaces.ants import MeasureImageSimilarity

sim = MeasureImageSimilarity()
sim.inputs.dimension = 3
sim.inputs.metric = 'MI'                            #('CC' or 'MI' or 'Mattes' or 'MeanSquares' or 'Demons' or'GC')
sim.inputs.fixed_image = 'train12\copd1\copd1_iBHCT.nii.gz'
sim.inputs.moving_image = 'train12\copd1\copd1_eBHCT.nii.gz'
sim.inputs.metric_weight = 1.0
sim.inputs.radius_or_number_of_bins = 5
sim.inputs.sampling_strategy = 'Regular'
sim.inputs.sampling_percentage = 1.0
#sim.inputs.fixed_image_mask = 'mask.nii'
#sim.inputs.moving_image_mask = 'mask.nii.gz'
sim.cmdline

sim.run()



#--------------------------------  Registration  --------------------------------------#
#  https://nipype.readthedocs.io/en/latest/interfaces/generated/interfaces.ants/registration.html

import copy, pprint
from nipype.interfaces.ants import Registration
reg = Registration()
reg.inputs.fixed_image = 'train12\copd1\copd1_iBHCT.nii.gz'
reg.inputs.moving_image = 'train12\copd1\copd1_eBHCT.nii.gz'
reg.inputs.output_transform_prefix = "output_"
#reg.inputs.initial_moving_transform = 'trans.mat'
reg.inputs.transforms = ['Affine', 'SyN']
reg.inputs.transform_parameters = [(2.0,), (0.25, 3.0, 0.0)]
reg.inputs.number_of_iterations = [[1500, 200], [100, 50, 30]]
reg.inputs.dimension = 3
reg.inputs.write_composite_transform = True
reg.inputs.collapse_output_transforms = False
reg.inputs.initialize_transforms_per_stage = False
reg.inputs.metric = ['Mattes']*2
reg.inputs.metric_weight = [1]*2 # Default (value ignored currently by ANTs)
reg.inputs.radius_or_number_of_bins = [32]*2
reg.inputs.sampling_strategy = ['Random', None]
reg.inputs.sampling_percentage = [0.05, None]
reg.inputs.convergence_threshold = [1.e-8, 1.e-9]
reg.inputs.convergence_window_size = [20]*2
reg.inputs.smoothing_sigmas = [[1,0], [2,1,0]]
reg.inputs.sigma_units = ['vox'] * 2
reg.inputs.shrink_factors = [[2,1], [3,2,1]]
reg.inputs.use_estimate_learning_rate_once = [True, True]
reg.inputs.use_histogram_matching = [True, True] # This is the default
reg.inputs.output_warped_image = 'output_warped_image.nii.gz'
reg.cmdline
reg.run()  # doctest: +SKIP










# rotate image volume anti-clockwise 90 degrees, 'k' number of times
img_in_data_rot = np.rot90(img_in_data, k = 3)

# to save this 3D (ndarry) numpy use this
in_dots_nifti_img = nib.Nifti1Image(img_in_data_rot, img_in.affine)

nib.save(in_dots_nifti_img, 'output.nii.gz')








