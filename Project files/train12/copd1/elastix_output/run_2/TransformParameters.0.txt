(Transform "AffineTransform")
(NumberOfParameters 12)
(TransformParameters 0.999986 -0.000079 -0.000019 0.000029 0.999996 0.000099 -0.007808 0.006101 0.946611 -0.010276 0.027374 3.979245)
(InitialTransformParametersFileName "NoInitialTransform")
(UseBinaryFormatForTransformationParameters "false")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 512 512 121)
(Index 0 0 0)
(Spacing 0.6200000048 0.6200000048 2.5000000000)
(Origin 0.0000000000 0.0000000000 0.0000000000)
(Direction -1.0000000000 0.0000000000 0.0000000000 0.0000000000 1.0000000000 0.0000000000 0.0000000000 0.0000000000 -1.0000000000)
(UseDirectionCosines "true")

// AdvancedAffineTransform specific
(CenterOfRotationPoint -158.4100012183 158.4100012183 -150.0000000000)

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 3)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "mhd")
(ResultImagePixelType "short")
(CompressResultImage "true")
